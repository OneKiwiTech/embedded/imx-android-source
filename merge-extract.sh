#!/bin/bash

if ! [ -d imx-android-14.0.0_1.0.0 ]; then
    echo "merge imx-android-14.0.0_1.0.0.tar.gz"
    cat imx-android-14.0.0_1.0.0.tar.gz.* > imx-android-14.0.0_1.0.0.tar.gz

    echo "extract imx-android-14.0.0_1.0.0.tar.gz"
    tar xzvf imx-android-14.0.0_1.0.0.tar.gz
fi

# split --bytes=50MB imx-android-14.0.0_1.0.0.tar.gz imx-android-14.0.0_1.0.0.tar.gz.
# cat imx-android-14.0.0_1.0.0.tar.gz.* > imx-android-14.0.0_1.0.0.tar.gz
# tar xzvf imx-android-14.0.0_1.0.0.tar.gz