#!/bin/bash

if [ -d imx-android-14.0.0_1.0.0 ]; then
    echo "compress imx-android-14.0.0_1.0.0.tar.gz"
    tar -czvf imx-android-14.0.0_1.0.0.tar.gz imx-android-14.0.0_1.0.0
fi

if [ -f imx-android-14.0.0_1.0.0.tar.gz ]; then
    echo "split imx-android-14.0.0_1.0.0.tar.gz"
    #split --bytes=50MB imx-android-14.0.0_1.0.0.tar.gz imx-android-14.0.0_1.0.0.tar.gz.
    split --bytes=50MB --numeric-suffixes=1 imx-android-14.0.0_1.0.0.tar.gz imx-android-14.0.0_1.0.0.tar.gz.
fi
