#!/bin/bash

if ! [ -d toolchain ]; then
    mkdir toolchain
    cd toolchain

    echo "download gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu.tar.xz"
    wget https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu.tar.xz
    echo "extract gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu.tar.xz"
    tar -xvJf gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu.tar.xz

    echo "clone prebuilt-android-clang"
    git clone https://android.googlesource.com/platform/prebuilts/clang/host/linux-x86 prebuilt-android-clang
    cd prebuilt-android-clang
    git checkout d20e409261d6ad80a0c29ac2055bf5c3bb996ef4
    cd ../

     echo "clone prebuilt-android-kernel-build-tools"
    git clone https://android.googlesource.com/kernel/prebuilts/build-tools prebuilt-android-kernel-build-tools
    cd prebuilt-android-kernel-build-tools
    git checkout e3f6a8c059b94f30f7184a7d335876f8a13a2366
    cd ../
fi

if ! [ -d android_build ]; then
    echo "Getting i.MX Android release source code"
    source imx-android-14.0.0_1.0.0/imx_android_setup.sh
fi