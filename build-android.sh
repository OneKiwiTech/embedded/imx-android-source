#!/bin/bash

./merge-extract.sh

./toolchain.sh

export AARCH64_GCC_CROSS_COMPILE=$(pwd)/toolchain/gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu/bin/aarch64-none-linux-gnu-
export CLANG_PATH=$(pwd)/toolchain/prebuilt-android-clang
export PATH=$(pwd)/toolchain/prebuilt-android-kernel-build-tools/linux-x86/bin:$PATH

cd android_build/
source build/envsetup.sh
lunch mek_8q-userdebug

# Building U-Boot images
#./imx-make.sh bootloader --j$(nproc)
# Building a kernel image
#./imx-make.sh kernel -c --j$(nproc)
# Building boot.img
#./imx-make.sh bootimage --j$(nproc)
# Building dtbo.img
#./imx-make.sh dtboimage --j$(nproc)